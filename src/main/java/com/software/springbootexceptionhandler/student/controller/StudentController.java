package com.software.springbootexceptionhandler.student.controller;

import com.software.springbootexceptionhandler.student.model.Student;
import com.software.springbootexceptionhandler.student.service.GeneralService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/students")
@RequiredArgsConstructor
public class StudentController {

    private final GeneralService<Student> generalService;

    @GetMapping
    public ResponseEntity<?> select() {
        return ResponseEntity.ok()
                .body(generalService.select());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> selectById(@PathVariable("id") Long id) {
        return ResponseEntity.ok()
                .body(generalService.selectById(id));
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestBody Student student) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(generalService.save(student));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {

        generalService.delete(id);

        return ResponseEntity.ok()
                .build();
    }
}
