package com.software.springbootexceptionhandler.student.service;

import com.software.springbootexceptionhandler.student.exception.IllegalNumberException;
import com.software.springbootexceptionhandler.student.exception.StudentNotFoundException;
import com.software.springbootexceptionhandler.student.model.Student;
import com.software.springbootexceptionhandler.student.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentService implements GeneralService<Student> {

    private final StudentRepository studentRepository;

    @Value("${student.exception.not_found}")
    private String STUDENT_NOT_FOUND;

    @Value("${student.exception.illegal_id}")
    private String STUDENT_ILLEGAL_ID;

    @Override
    public List<Student> select() {
        return studentRepository.findAll();
    }

    @Override
    public Student selectById(Long id) {

        return studentRepository.findById(id)
                .orElseThrow(() ->
                        new StudentNotFoundException
                                (String.format(STUDENT_NOT_FOUND, id)));
    }

    @Override
    public Student save(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public void delete(Long id) {

        if (id < 1) {
            throw new IllegalNumberException(String.format(STUDENT_ILLEGAL_ID, id));
        }
        studentRepository.delete(selectById(id));
    }
}
