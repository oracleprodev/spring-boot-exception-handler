package com.software.springbootexceptionhandler.student.service;

import java.util.List;

public interface GeneralService<T> {
    List<T> select();
    T selectById(Long id);
    T save(T t);
    void delete(Long id);
}
